#!/bin/python3

import math
import os
import random
import re
import sys

#
# Complete the 'equalizeArray' function below.
#
# The function is expected to return an INTEGER.
# The function accepts INTEGER_ARRAY arr as parameter.
#

def equalizeArray(arr):
    count = {}
    highest = 0
    for num in arr:
        if num not in count:
            count[num] = 1
        else:
            count[num] += 1
        if highest == 0:
            highest = num
        elif highest != num:
            if count[highest] < count[num]:
                highest = num
    return len(arr) - count[highest]

if __name__ == '__main__':
    fptr = open(os.environ['OUTPUT_PATH'], 'w')

    n = int(input().strip())

    arr = list(map(int, input().rstrip().split()))

    result = equalizeArray(arr)

    fptr.write(str(result) + '\n')

    fptr.close()
