#!/bin/python3

import os
import sys

#
# Complete the getMoneySpent function below.
#
def getMoneySpent(keyboards, drives, b):
    p = [0,0]
    maximum = -1
    while p[0] < len(keyboards):
        if keyboards[p[0]] + drives[p[1]] <= b and keyboards[p[0]] + drives[p[1]] > maximum:
            maximum = keyboards[p[0]] + drives[p[1]]
        p[1] += 1
        if p[1] == len(drives):
            p[1] = 0
            p[0] += 1
    return maximum

if __name__ == '__main__':
    fptr = open(os.environ['OUTPUT_PATH'], 'w')

    bnm = input().split()

    b = int(bnm[0])

    n = int(bnm[1])

    m = int(bnm[2])

    keyboards = list(map(int, input().rstrip().split()))

    drives = list(map(int, input().rstrip().split()))

    #
    # The maximum amount of money she can spend on a keyboard and USB drive, or -1 if she can't purchase both items
    #

    moneySpent = getMoneySpent(keyboards, drives, b)

    fptr.write(str(moneySpent) + '\n')

    fptr.close()
