#!/bin/python3

import math
import os
import random
import re
import sys

#
# Complete the 'jumpingOnClouds' function below.
#
# The function is expected to return an INTEGER.
# The function accepts INTEGER_ARRAY c as parameter.
#

def jumpingOnClouds(c):
    jumps = 0
    currentIndex = 0
    THUNDERHEAD = 1
    CUMULUS = 0
    while len(c) > 1:
        thunderheadIndex = -1
        if THUNDERHEAD in c:
            thunderheadIndex = c.index(THUNDERHEAD)
        if thunderheadIndex >= 1:
            jumps += math.floor(thunderheadIndex / 2)
            c = c[thunderheadIndex:]
        if c[0] == THUNDERHEAD:
            jumps += 1
            c = c[1:]
        if thunderheadIndex == -1:
            jumps += math.floor(len(c) / 2)
            break
    return jumps

if __name__ == '__main__':
    fptr = open(os.environ['OUTPUT_PATH'], 'w')

    n = int(input().strip())

    c = list(map(int, input().rstrip().split()))

    result = jumpingOnClouds(c)

    fptr.write(str(result) + '\n')

    fptr.close()
