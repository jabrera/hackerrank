#!/bin/python3

import math
import os
import random
import re
import sys

#
# Complete the 'migratoryBirds' function below.
#
# The function is expected to return an INTEGER.
# The function accepts INTEGER_ARRAY arr as parameter.
#

def migratoryBirds(arr):
    birds = {}
    for bird in arr:
        if bird in birds:
            birds[bird] = birds[bird] + 1
        else:
            birds[bird] = 1
    highest = [0,0]
    for bird in birds:
        if birds[bird] > highest[1]:
            highest = [bird, birds[bird]]
        elif birds[bird] == highest[1]:
            if bird < highest[0] or highest[0] == 0:
                highest = [bird, birds[bird]]
    return highest[0]

if __name__ == '__main__':
    fptr = open(os.environ['OUTPUT_PATH'], 'w')

    arr_count = int(input().strip())

    arr = list(map(int, input().rstrip().split()))

    result = migratoryBirds(arr)

    fptr.write(str(result) + '\n')

    fptr.close()
