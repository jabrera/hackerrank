#!/bin/python3

import math
import os
import random
import re
import sys

#
# Complete the 'climbingLeaderboard' function below.
#
# The function is expected to return an INTEGER_ARRAY.
# The function accepts following parameters:
#  1. INTEGER_ARRAY ranked
#  2. INTEGER_ARRAY player
#

def climbingLeaderboard(ranked, playerScores):
    output = []
    lastIndex = len(ranked) - 1
    lastRank = 0
    previousScore = None
    for r in ranked:
        if r == previousScore:
            continue
        previousScore = r
        lastRank += 1
    for playerScore in playerScores:
        while True:
            value = ranked[lastIndex]
            if value > playerScore:
                lastIndex += 1
                lastRank += 1
                ranked.insert(lastIndex, playerScore)
                break
            elif value == playerScore:
                break
            if lastIndex + 1 != len(ranked):
                if value != ranked[lastIndex + 1]:
                    lastRank -= 1
            else:
                lastRank -= 1
            lastIndex -= 1
            
            if lastIndex == -1:
                lastIndex = 0
                lastRank = 1
                ranked.insert(lastIndex, playerScore)
                break
        output.append(lastRank)
    return output

if __name__ == '__main__':
    fptr = open(os.environ['OUTPUT_PATH'], 'w')

    ranked_count = int(input().strip())

    ranked = list(map(int, input().rstrip().split()))

    player_count = int(input().strip())

    player = list(map(int, input().rstrip().split()))

    result = climbingLeaderboard(ranked, player)

    fptr.write('\n'.join(map(str, result)))
    fptr.write('\n')

    fptr.close()
