#!/bin/python3

import math
import os
import random
import re
import sys

#
# Complete the 'beautifulDays' function below.
#
# The function is expected to return an INTEGER.
# The function accepts following parameters:
#  1. INTEGER i
#  2. INTEGER j
#  3. INTEGER k
#

def beautifulDays(i, j, k):
    numberOfBeautifulDays = 0
    for day in range(i,j+1):
        reverse_day = str(day)[::-1]
        abs_diff = abs(day - int(reverse_day))
        if abs_diff % k == 0: 
            numberOfBeautifulDays = numberOfBeautifulDays + 1
    return numberOfBeautifulDays

if __name__ == '__main__':
    fptr = open(os.environ['OUTPUT_PATH'], 'w')

    first_multiple_input = input().rstrip().split()

    i = int(first_multiple_input[0])

    j = int(first_multiple_input[1])

    k = int(first_multiple_input[2])

    result = beautifulDays(i, j, k)

    fptr.write(str(result) + '\n')

    fptr.close()
