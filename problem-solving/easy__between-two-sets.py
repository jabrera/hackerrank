#!/bin/python3

import math
import os
import random
import re
import sys

#
# Complete the 'getTotalX' function below.
#
# The function is expected to return an INTEGER.
# The function accepts following parameters:
#  1. INTEGER_ARRAY a
#  2. INTEGER_ARRAY b
#

def getTotalX(a, b):
    a.sort()
    factorials = []
    for i in range(max(a+b) + 1):
        if i == 0:
            continue
        isFactor = True
        for v in a:
            if i % v != 0:
                isFactor = False
                break
        if not isFactor:
            continue
        for v in b:
            if v % i != 0:
                isFactor = False
                break
        if not isFactor:
            continue
        factorials.append(i)
    return len(factorials)
            
    # Write your code here

if __name__ == '__main__':
    fptr = open(os.environ['OUTPUT_PATH'], 'w')

    first_multiple_input = input().rstrip().split()

    n = int(first_multiple_input[0])

    m = int(first_multiple_input[1])

    arr = list(map(int, input().rstrip().split()))

    brr = list(map(int, input().rstrip().split()))

    total = getTotalX(arr, brr)

    fptr.write(str(total) + '\n')

    fptr.close()
