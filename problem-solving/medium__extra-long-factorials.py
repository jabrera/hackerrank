#!/bin/python3

import math
import os
import random
import re
import sys

#
# Complete the 'extraLongFactorials' function below.
#
# The function accepts INTEGER n as parameter.
#

def extraLongFactorials(n):
    arr = list(range(1,n))
    arr.reverse()
    factorial = n
    for x in arr:
        factorial = factorial * x
    print(factorial)

if __name__ == '__main__':
    n = int(input().strip())

    extraLongFactorials(n)
