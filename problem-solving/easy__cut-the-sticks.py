#!/bin/python3

import math
import os
import random
import re
import sys

#
# Complete the 'cutTheSticks' function below.
#
# The function is expected to return an INTEGER_ARRAY.
# The function accepts INTEGER_ARRAY arr as parameter.
#

def cutTheSticks(arr):
    values = {}
    for num in arr:
        if num in values:
            values[num] += 1
        else:
            values[num] = 1
            
    length_per_loop = []
    total_length = len(arr)
    length_per_loop.append(total_length)
    unique_arr = set(arr)
    unique_arr = sorted(unique_arr)
    for num in unique_arr:
        total_length -= values[num]
        if total_length != 0:
            length_per_loop.append(total_length)
    return length_per_loop

if __name__ == '__main__':
    fptr = open(os.environ['OUTPUT_PATH'], 'w')

    n = int(input().strip())

    arr = list(map(int, input().rstrip().split()))

    result = cutTheSticks(arr)

    fptr.write('\n'.join(map(str, result)))
    fptr.write('\n')

    fptr.close()
