#!/bin/python3

import math
import os
import random
import re
import sys

#
# Complete the 'breakingRecords' function below.
#
# The function is expected to return an INTEGER_ARRAY.
# The function accepts INTEGER_ARRAY scores as parameter.
#

def breakingRecords(scores):
    minMaxCount = [0,0]
    minMaxScore = [0,0]
    for index, score in enumerate(scores):
        if index == 0:
            minMaxScore = [score, score]
            continue
        if score > minMaxScore[0]:
            minMaxScore[0] = score
            minMaxCount[0] = minMaxCount[0] + 1
        if score < minMaxScore[1]:
            minMaxScore[1] = score
            minMaxCount[1] = minMaxCount[1] + 1
    return minMaxCount

if __name__ == '__main__':
    fptr = open(os.environ['OUTPUT_PATH'], 'w')

    n = int(input().strip())

    scores = list(map(int, input().rstrip().split()))

    result = breakingRecords(scores)

    fptr.write(' '.join(map(str, result)))
    fptr.write('\n')

    fptr.close()
