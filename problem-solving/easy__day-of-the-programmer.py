#!/bin/python3

import math
import os
import random
import re
import sys
from datetime import datetime, timedelta

#
# Complete the 'dayOfProgrammer' function below.
#
# The function is expected to return a STRING.
# The function accepts INTEGER year as parameter.
#

def dayOfProgrammer(year):
    daysToDayOfProgrammer = 255
    date = datetime(year,1,1)
    if year < 1918:
        if year % 100 == 0:
            daysToDayOfProgrammer = daysToDayOfProgrammer - 1
    elif year == 1918:
        daysToDayOfProgrammer = daysToDayOfProgrammer + 13
    return (date + timedelta(days=daysToDayOfProgrammer)).strftime("%d.%m.%Y")

if __name__ == '__main__':
    fptr = open(os.environ['OUTPUT_PATH'], 'w')

    year = int(input().strip())

    result = dayOfProgrammer(year)

    fptr.write(result + '\n')

    fptr.close()
