#!/bin/python3

import math
import os
import random
import re
import sys

#
# Complete the 'kaprekarNumbers' function below.
#
# The function accepts following parameters:
#  1. INTEGER p
#  2. INTEGER q
#

def kaprekarNumbers(p, q):
    output = []
    for i in range(p,q+1):
        d = len(str(i))
        square = str(math.floor(math.pow(i,2)))
        if(int(square[:0-d] if square[:0-d] != "" else 0) + int(square[0-d:] if square[0-d:] != "" else 0) == i):
            output.append(str(i))
    if len(output) > 0:
        print(" ".join(output))
    else:
        print("INVALID RANGE")

if __name__ == '__main__':
    p = int(input().strip())

    q = int(input().strip())

    kaprekarNumbers(p, q)
