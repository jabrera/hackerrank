#!/bin/python3

import math
import os
import random
import re
import sys

#
# Complete the 'pickingNumbers' function below.
#
# The function is expected to return an INTEGER.
# The function accepts INTEGER_ARRAY a as parameter.
#

def pickingNumbers(a):
    instances = {}
    maximumLength = 0
    for num in a:
        if num in instances:
            instances[num] = instances[num] + 1
        else:
            instances[num] = 1
    for num in instances:
        sum_of_pair = instances[num]
        if num+1 in instances:
            sum_of_pair = sum_of_pair + instances[num+1]
        if sum_of_pair > maximumLength:
            maximumLength = sum_of_pair
    return maximumLength
    # get the highest sum
    
    # Write your code here

if __name__ == '__main__':
    fptr = open(os.environ['OUTPUT_PATH'], 'w')

    n = int(input().strip())

    a = list(map(int, input().rstrip().split()))

    result = pickingNumbers(a)

    fptr.write(str(result) + '\n')

    fptr.close()
