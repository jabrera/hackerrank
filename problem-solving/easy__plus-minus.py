#!/bin/python3

import math
import os
import random
import re
import sys

#
# Complete the 'plusMinus' function below.
#
# The function accepts INTEGER_ARRAY arr as parameter.
#

def plusMinus(arr):
    ratio = [0,0,0]
    for x in arr:
        if x > 0:
            ratio[0] = ratio[0] + 1
        if x < 0:
            ratio[1] = ratio[1] + 1
        if x == 0:
            ratio[2] = ratio[2] + 1
    for x in ratio:
        print(str(round(x / len(arr), 6)).ljust(8, "0"))

if __name__ == '__main__':
    n = int(input().strip())

    arr = list(map(int, input().rstrip().split()))

    plusMinus(arr)
