#!/bin/python3

import math
import os
import random
import re
import sys

#
# Complete the 'beautifulTriplets' function below.
#
# The function is expected to return an INTEGER.
# The function accepts following parameters:
#  1. INTEGER d
#  2. INTEGER_ARRAY arr
#

def beautifulTriplets(d, arr):
    if len(arr) < 3:
        return 0
    num = 0
    tripletA = 0
    tripletB = 1
    tripletC = 2
    while tripletA != len(arr) - 2:
        if arr[tripletB] - arr[tripletA] == d and arr[tripletC] - arr[tripletB] == d:
            num += 1
        if arr[tripletC] - arr[tripletB] > d:
            tripletB += 1
            tripletC = tripletB + 1
            if tripletB == len(arr) - 1:
                tripletA += 1
                tripletB = tripletA + 1
                tripletC = tripletB + 1
            continue
        if arr[tripletB] - arr[tripletA] > d:
            tripletA += 1
            tripletB = tripletA + 1
            tripletC = tripletB + 1
            continue
        tripletC += 1
        if tripletC == len(arr):
            tripletB += 1
            tripletC = tripletB + 1
        if tripletB == len(arr) - 1:
            tripletA += 1
            tripletB = tripletA + 1
            tripletC = tripletB + 1
            
    return num

if __name__ == '__main__':
    fptr = open(os.environ['OUTPUT_PATH'], 'w')

    first_multiple_input = input().rstrip().split()

    n = int(first_multiple_input[0])

    d = int(first_multiple_input[1])

    arr = list(map(int, input().rstrip().split()))

    result = beautifulTriplets(d, arr)

    fptr.write(str(result) + '\n')

    fptr.close()
