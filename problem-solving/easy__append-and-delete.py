#!/bin/python3

import math
import os
import random
import re
import sys

#
# Complete the 'appendAndDelete' function below.
#
# The function is expected to return a STRING.
# The function accepts following parameters:
#  1. STRING s
#  2. STRING t
#  3. INTEGER k
#

def appendAndDelete(s, t, k):
    step = 0
    while step < k:
        if s != t[0:len(s)]:
            s = s[:-1]
        else:
            if len(t[len(s):]) == k-step:
                return "Yes"
            if s == t and len(s) * 2 <= k-step:
                return "Yes"
            if len(t[len(s):]) < k-step:
                extraStepsAvailable = k-step - len(t[len(s):])
                if extraStepsAvailable % 2 == 0 or (s == "" and k-step >= len(t)):
                    return "Yes"
                if len(t[len(s):]) % 2 == (k-step) % 2:
                    return "Yes"
                return "No"
        step = step + 1
    return "Yes" if k == step and s == t else "No"

if __name__ == '__main__':
    fptr = open(os.environ['OUTPUT_PATH'], 'w')

    s = input()

    t = input()

    k = int(input().strip())

    result = appendAndDelete(s, t, k)

    fptr.write(result + '\n')

    fptr.close()
