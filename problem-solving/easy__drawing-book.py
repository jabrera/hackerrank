#!/bin/python3

import math
import os
import random
import re
import sys

#
# Complete the 'pageCount' function below.
#
# The function is expected to return an INTEGER.
# The function accepts following parameters:
#  1. INTEGER n
#  2. INTEGER p
#

def pageCount(n, p):
    pages = list(range(n + 1 + (1 if n % 2 == 0 else 0)))
    middle = math.ceil((len(pages)-1) / 2)
    if middle % 2 == 0:
        middle = middle - 1
    if p > middle:
        pages.reverse()
    index = pages.index(p)
    flip = index // 2
    return flip

if __name__ == '__main__':
    fptr = open(os.environ['OUTPUT_PATH'], 'w')

    n = int(input().strip())

    p = int(input().strip())

    result = pageCount(n, p)

    fptr.write(str(result) + '\n')

    fptr.close()
