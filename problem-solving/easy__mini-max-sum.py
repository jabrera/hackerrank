#!/bin/python3

import math
import os
import random
import re
import sys

#
# Complete the 'miniMaxSum' function below.
#
# The function accepts INTEGER_ARRAY arr as parameter.
#

def miniMaxSum(arr):
    arr.sort()
    minSum = 0
    maxSum = 0
    for key, value in enumerate(arr):
        if key == 0:
            minSum = minSum + value
            continue
        elif key == len(arr) - 1:
            maxSum = maxSum + value
            continue
        minSum = minSum + value
        maxSum = maxSum + value
    print(minSum, maxSum)

if __name__ == '__main__':

    arr = list(map(int, input().rstrip().split()))

    miniMaxSum(arr)
