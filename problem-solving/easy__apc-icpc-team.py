#!/bin/python3

import math
import os
import random
import re
import sys

#
# Complete the 'acmTeam' function below.
#
# The function is expected to return an INTEGER_ARRAY.
# The function accepts STRING_ARRAY topic as parameter.
#

def acmTeam(topic):
    print(topic)
    output = [0, 0]
    memberA = 0
    memberB = 1
    while memberA + 1 < len(topic):
        totalTopics = len(topic[memberA]) - str(int(topic[memberA]) + int(topic[memberB])).ljust(len(topic[memberA]), "0").count("0")
        if totalTopics > output[0]:
            output = [totalTopics, 0]
        if totalTopics == output[0]:
            output[1] += 1
        memberB += 1
        if memberB == len(topic):
            memberA += 1
            memberB = memberA + 1
    return output
    # Write your code here

if __name__ == '__main__':
    fptr = open(os.environ['OUTPUT_PATH'], 'w')

    first_multiple_input = input().rstrip().split()

    n = int(first_multiple_input[0])

    m = int(first_multiple_input[1])

    topic = []

    for _ in range(n):
        topic_item = input()
        topic.append(topic_item)

    result = acmTeam(topic)

    fptr.write('\n'.join(map(str, result)))
    fptr.write('\n')

    fptr.close()
